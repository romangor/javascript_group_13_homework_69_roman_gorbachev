import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ApplicationService } from 'src/app/shared/application.service';
import { Application } from '../../shared/aplication.model';

@Component({
  selector: 'app-application-list',
  templateUrl: './application-list.component.html',
  styleUrls: ['./application-list.component.css']
})
export class ApplicationListComponent implements OnInit, OnDestroy {
  applications!: Application[];
  applicationSubscription!: Subscription;
  loadingApplications: boolean = false;
  constructor(private applicationService: ApplicationService) { }

  ngOnInit(): void {
    this.applicationService.loadingApplications.subscribe( (isLoading: boolean) => {
      this.loadingApplications = isLoading;
    })
    this.applicationService.fetchApplications();
    this.applicationSubscription = this.applicationService.applicationsChange.subscribe( applications => {
      this.applications = applications;
    })
  }

  ngOnDestroy() {
    this.applicationSubscription.unsubscribe();
  }

}
