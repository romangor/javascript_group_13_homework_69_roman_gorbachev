import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Application } from 'src/app/shared/aplication.model';
import { ApplicationService } from '../../shared/application.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-application-details',
  templateUrl: './application-details.component.html',
  styleUrls: ['./application-details.component.css']
})
export class ApplicationDetailsComponent implements OnInit, OnDestroy {
  application!: Application;
  removing: boolean = false;
  removingSubscription!: Subscription;
  constructor(private route: ActivatedRoute,
              private applicationService: ApplicationService) { }

  ngOnInit(): void {
    this.route.data.subscribe(data =>{
      this.application = <Application>data.application;
    });
    this.removingSubscription = this.applicationService.removingApplications.subscribe( (isRemoving: boolean) => {
      this.removing = isRemoving
    })
  }

  remove() {
    this.applicationService.removeApplication(this.application.id);
  }

  ngOnDestroy() {
    this.removingSubscription.unsubscribe();
  }
}
