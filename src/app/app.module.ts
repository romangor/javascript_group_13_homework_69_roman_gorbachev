import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FormRegistrationComponent } from './form-registration/form-registration.component';
import { FormsModule } from '@angular/forms';
import { ValidatePhoneNumberDirective } from './directives/validate-phone-number.directive';
import { ApplicationService } from './shared/application.service';
import { HttpClientModule } from '@angular/common/http';
import { SuccessSubmissionComponent } from './successful-submission.component';
import { ApplicationListComponent } from './home/application-list/application-list.component';
import { ApplicationDetailsComponent } from './home/application-details/application-details.component';
import { ToolbarComponent } from './toolbar/toolbar.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FormRegistrationComponent,
    ValidatePhoneNumberDirective,
    SuccessSubmissionComponent,
    ApplicationListComponent,
    ApplicationDetailsComponent,
    ToolbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [ApplicationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
