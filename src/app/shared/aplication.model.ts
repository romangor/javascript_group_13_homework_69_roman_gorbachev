export class Application {
  constructor(public firstName: string,
              public lastName: string,
              public middleName: string,
              public phoneNumber: string,
              public placeOfWork: string,
              public gender: string,
              public size: string,
              public comment: string,
              public id: string){}
}
