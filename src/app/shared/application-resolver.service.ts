import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { Application } from './aplication.model';
import { ApplicationService } from './application.service';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApplicationResolverService implements Resolve<Application>{
  constructor(private applicationService: ApplicationService,
              private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Application> | Observable<never> {
    const id = <string>route.params['id'];
    return this.applicationService.fetchApplication(id).pipe(mergeMap(application => {
      if(application) {
        return of(application);
      } else {
        void this.router.navigate(['']);
        return EMPTY;
      }
    }));
  }
}
