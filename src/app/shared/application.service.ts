import { Application } from './aplication.model';

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class ApplicationService {
  applicationsChange = new Subject<Application[]>();
  loadingApplications = new Subject<boolean>();
  sendingApplications = new Subject<boolean>();
  removingApplications = new Subject<boolean>();
  applications: Application[] = [];

  constructor(private http: HttpClient,
              private router: Router) {
  }

  sendApplication(application: Application) {
    this.sendingApplications.next(true);
    const body = {
      firstName: application.firstName,
      lastName: application.lastName,
      middleName: application.middleName,
      phoneNumber: application.phoneNumber,
      placeOfWork: application.placeOfWork,
      gender: application.gender,
      size: application.size,
      comment: application.comment,
    }
    this.http.post('https://projectsattractor-default-rtdb.firebaseio.com/applications.json', body).subscribe(() => {
      this.sendingApplications.next(true);
      void this.router.navigate(['/successful']);
    });
  }

  editApplication(application: Application){
    this.sendingApplications.next(true);
    const body = {
      firstName: application.firstName,
      lastName: application.lastName,
      middleName: application.middleName,
      phoneNumber: application.phoneNumber,
      placeOfWork: application.placeOfWork,
      gender: application.gender,
      size: application.size,
      comment: application.comment,
    }
    this.http.put(`https://projectsattractor-default-rtdb.firebaseio.com/applications/${application.id}.json`, body).subscribe( () => {
      this.sendingApplications.next(false);
      void this.router.navigate(['']);
    });
  }

  fetchApplications(){
    this.loadingApplications.next(true);
    this.http.get<{[id: string]: Application}>('https://projectsattractor-default-rtdb.firebaseio.com/applications.json')
      .pipe(map(result =>{
        if (result === null){
          return [];
        }
        return Object.keys(result).map(id =>{
          const applicationData = result[id];
          return new Application(
            applicationData.firstName,
            applicationData.lastName,
            applicationData.middleName,
            applicationData.phoneNumber,
            applicationData.placeOfWork,
            applicationData.gender,
            applicationData.size,
            applicationData.comment,
            id);
        });
    }))
      .subscribe(applications =>{
        this.applications = applications;
        this.applicationsChange.next(this.applications.slice());
        this.loadingApplications.next(false);
      })
  }

  fetchApplication(id: string){
    this.loadingApplications.next(false);
    return this.http.get<Application | null>(`https://projectsattractor-default-rtdb.firebaseio.com/applications/${id}.json`)
      .pipe(map(result => {
        if (!result) return null;
        return new Application(
          result.firstName,
          result.lastName,
          result.middleName,
          result.phoneNumber,
          result.placeOfWork,
          result.gender,
          result.size,
          result.comment,
          id);
      }))
  }

  removeApplication(id: string){
    this.removingApplications.next(true);
   return this.http.delete(`https://projectsattractor-default-rtdb.firebaseio.com/applications/${id}.json`).subscribe(() => {
     this.fetchApplications();
     this.removingApplications.next(false);
   })
  }
}
