import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormRegistrationComponent } from './form-registration/form-registration.component';
import { HomeComponent } from './home/home.component';
import { SuccessSubmissionComponent } from './successful-submission.component';
import { ApplicationDetailsComponent } from './home/application-details/application-details.component';
import { ApplicationResolverService } from './shared/application-resolver.service';


const routes: Routes = [
  {path: '', component: HomeComponent, children: [
      {path: 'successful', component: SuccessSubmissionComponent},
      {path: ':id', component: ApplicationDetailsComponent,
      resolve: {
        application: ApplicationResolverService
      }},
      {path: ':id/edit', component: FormRegistrationComponent,
        resolve: {
        application: ApplicationResolverService
      }},
    ]},
  {path: 'fill/app', component: FormRegistrationComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
