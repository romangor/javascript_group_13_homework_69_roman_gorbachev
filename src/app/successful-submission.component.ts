import { Component } from '@angular/core';

@Component({
  selector: 'successful-submission',
  template: `<h1>Thank you for your application</h1>`,
  styles: [`
    h1 {
      color: black;
    }
  `]
})
export class SuccessSubmissionComponent {}
