import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { Directive } from '@angular/core';

@Directive({
  selector: '[appPhoneNumber]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidatePhoneNumberDirective,
    multi: true
  }]
})

export class ValidatePhoneNumberDirective implements  Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return numberValidator()(control)
  }
}

export  function numberValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    const numberPhone = (/(\+996)[\s]*\d{3}[\s]*\d{2}\d{2}\d{2}/).test(control.value);
    if (numberPhone) {
      return null;
    }
    return  {number: true};
  }
}

