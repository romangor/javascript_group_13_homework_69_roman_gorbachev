import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ApplicationService } from 'src/app/shared/application.service';
import { Application } from '../shared/aplication.model';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-form-registration',
  templateUrl: './form-registration.component.html',
  styleUrls: ['./form-registration.component.css']
})
export class FormRegistrationComponent implements OnInit {
 @ViewChild('f') form!: NgForm;
  loading: boolean = false;
  isEdit = false;
  editId = '';
  loadingSubscription!: Subscription;
  constructor(private applicationService: ApplicationService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.loadingSubscription = this.applicationService.sendingApplications.subscribe( (isLoading: boolean) => {
      this.loading = isLoading;
    })
    this.route.data.subscribe(data => {
      const application = <Application | null>data.application;
      if (application) {
        this.setFormValue({
          firstName: application.firstName,
          lastName: application.lastName,
          middleName: application.middleName,
          phoneNumber: application.phoneNumber,
          placeOfWork: application.placeOfWork,
          gender: application.gender,
          size: application.size,
          comment: application.comment
        });
        this.isEdit = true;
        this.editId = application.id;
      } else {
        this.setFormValue({
          firstName: '',
          lastName: '',
          middleName: '',
          phoneNumber: '',
          placeOfWork: '',
          gender: '',
          size: '',
          comment: '',
        });
        this.isEdit = false;
        this.editId = '';
      }
    })
  }

  send() {
    const application =  new Application(
      this.form.value.firstName,
      this.form.value.lastName,
      this.form.value.middleName,
      this.form.value.phoneNumber,
      this.form.value.placeOfWork,
      this.form.value.gender,
      this.form.value.size,
      this.form.value.comment,
      this.form.value.id = '' || this.editId);
    if (this.isEdit) {
      this.applicationService.editApplication(application);
    } else {
      this.applicationService.sendApplication(application);
    }
}

  setFormValue(value: {[key: string]: any}) {
    setTimeout(() => {
      this.form.form.setValue(value);
    });
  }
}
